package com.example.fitzls.samples;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.niallkader.Coder;

public class CoderActivity extends AppCompatActivity {

    EditText txtMessage;
    Button btnEncrypt, btnDecrypt;
    Coder coder = new Coder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coder);

        txtMessage = (EditText)findViewById(R.id.txtMesage);

        btnEncrypt = (Button)findViewById(R.id.btnEncrypt);
        btnDecrypt = (Button)findViewById(R.id.btnDecrypt);

        btnEncrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = txtMessage.getText().toString();
                String encMsg = coder.encode(msg);
                txtMessage.setText(encMsg);
            }
        });

        btnDecrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = txtMessage.getText().toString();
                String decMsg = coder.decode(msg);
                txtMessage.setText(decMsg);
            }
        });

    }

}
