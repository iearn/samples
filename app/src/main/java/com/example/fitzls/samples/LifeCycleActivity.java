package com.example.fitzls.samples;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.fitzls.samples.model.User;

public class LifeCycleActivity extends AppCompatActivity {

    public static final String TAG = "LifeCycle";

    User user = new User();
    Button btnIntent, btnSave;
    EditText editText2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_life_cycle);
        Log.d(TAG, "onCreate().... " + user.getFirstName());

        if (savedInstanceState != null){
            user.setFirstName(savedInstanceState.getString("FIRST_NAME"));
        }

        final EditText editText2 = (EditText) findViewById(R.id.editText2);

        btnIntent = (Button)findViewById(R.id.btnIntent);
        btnSave = (Button) findViewById(R.id.btnSave);

        btnIntent.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                String url = "https://www.google.com";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user.setFirstName(editText2.getText().toString());
                Log.d(TAG, "User first name: " + user.getFirstName());
            }
        });

    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop().... ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume().... ");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause().... ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy().... ");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart().... ");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(TAG, "onSaveInstanceState...... ");
        outState.putString("FIRST_NAME", user.getFirstName());

    }
}
