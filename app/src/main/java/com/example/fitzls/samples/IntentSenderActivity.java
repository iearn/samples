package com.example.fitzls.samples;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class IntentSenderActivity extends AppCompatActivity {

    Button btnSend;
    EditText txtMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent_sender);
        // Has to be under this to use Var.
        // YOu have to cast findView Because it returns a View.
        btnSend = (Button)findViewById(R.id.btnSend);
        txtMessage = (EditText)findViewById(R.id.txtMesage);
        // in the paranthesis type new OnCli and type Enter to get default code.
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*
                String url = "https://www.google.com";
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                startActivity(intent);
                */

                String messageText = txtMessage.getText().toString();
                Intent intent = new Intent(Intent.ACTION_SEND);
                // setting the Mime type
                intent.setType("text/plain");
                // we got the message for the textArea and
                // then we are putting extra data into the text
                intent.putExtra(Intent.EXTRA_TEXT, messageText);
//              My app and my app key is bleow
                intent.putExtra(IntentReceiverActivity.My_TEXT_DATA, messageText);
                String  chooserTitle = "Which App would you like to handle this intent...";
                Intent choosenIntent = Intent.createChooser(intent, chooserTitle);
                startActivity(choosenIntent);
            }
        });

    }
}
