package com.example.fitzls.samples;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;

public class ImageSpinnerActivity extends AppCompatActivity {

    Spinner spinner;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_spinner);

        imageView = (ImageView)findViewById(R.id.imageView2);
        spinner = (Spinner)findViewById(R.id.spinner);

        spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = spinner.getSelectedItem().toString();
                changeImage(selectedItem);
            }
        });
    }

    private void changeImage(String selectedItem){
        int rscId = 0;

        switch (selectedItem){
            case "Desert":
                rscId = R.drawable.desert;
                break;
            case "Jellyfish":
                rscId = R.drawable.jellyfish;
                break;
            case "Koala":
                rscId = R.drawable.koala;
                break;
            case "Lighthouse":
                rscId = R.drawable.lighthouse;
                break;
            case "Penguins":
                rscId = R.drawable.penguins;
                break;
            case "Tulips":
                rscId = R.drawable.tulips;
                break;

        }

        imageView.setImageResource(rscId);

    }

}
