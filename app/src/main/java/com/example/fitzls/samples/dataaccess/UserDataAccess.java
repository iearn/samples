package com.example.fitzls.samples.dataaccess;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Switch;
import android.widget.Toast;

import com.example.fitzls.samples.UserSQLiteOpenHelper;
import com.example.fitzls.samples.model.User;

import java.util.ArrayList;

/**
 * Created by SkyFy on 11/15/2017.
 */

public class UserDataAccess {

    public static final String TAG = "UserDataAccess";
    private UserSQLiteOpenHelper dbHelper;
    private SQLiteDatabase db;

    public static final String TABLE_NAME = "users";
    public static final String COLUMN_USER_ID = "_id";
    public static final String COLUMNS_USER_FIRST_NAME = "user_first_name";
    public static final String COLUMNS_USER_EMAIL = "user_email";
    public static final String COLUMNS_USER_MUSIC = "user_music";
    public static final String COLUMNS_USER_ACTIVE = "user_active";

    public static final String TABLE_CREATE =
            String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, " +
                            "%s TEXT, " +
                            "%s TEXT, " +
                            "%s TEXT, " +
                            "%s TEXT)",
                    TABLE_NAME,
                    COLUMN_USER_ID,
                    COLUMNS_USER_FIRST_NAME,
                    COLUMNS_USER_EMAIL,
                    COLUMNS_USER_MUSIC,
                    COLUMNS_USER_ACTIVE
            );

    public static final String SELECT_ALL_STATEMENT =
            String.format("SELECT %s, %s, %s, %s, %s FROM %s",
            COLUMN_USER_ID,
            COLUMNS_USER_FIRST_NAME,
            COLUMNS_USER_EMAIL,
            COLUMNS_USER_MUSIC,
            COLUMNS_USER_ACTIVE,
            TABLE_NAME);

    public UserDataAccess(UserSQLiteOpenHelper dbHelper){

        this.dbHelper = dbHelper;
        this.db = this.dbHelper.getWritableDatabase();

    }

    public User insertUser(User user){

        ContentValues values = new ContentValues();
        values.put(COLUMNS_USER_FIRST_NAME, user.getFirstName());
        values.put(COLUMNS_USER_EMAIL, user.getEmail());
        values.put(COLUMNS_USER_MUSIC, user.getFavoriteMusic().toString());
        values.put(COLUMNS_USER_ACTIVE, user.isActive());

        long inserId = db.insert(TABLE_NAME, null, values);
        return user;

    }

    public ArrayList<User> getAllUsers(){

        ArrayList<User> users = new ArrayList<User>();
        String query = String.format("SELECT %s, %s, %s, %s, %s FROM %s",
                COLUMN_USER_ID,
                COLUMNS_USER_FIRST_NAME,
                COLUMNS_USER_EMAIL,
                COLUMNS_USER_MUSIC,
                COLUMNS_USER_ACTIVE,
                TABLE_NAME);

        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();
        while(!c.isAfterLast()){

            String userMusic = c.getString(3);
            User.Music um = null;

            Log.d(TAG, c.getString(3));

            if (userMusic.equalsIgnoreCase("Rap")){
                um = User.Music.RAP;
            }else if (userMusic.equalsIgnoreCase("Country")){
                um = User.Music.COUNTRY;
            }else if (userMusic.equalsIgnoreCase("Jazz")){
                um = User.Music.JAZZ;
            }

            String strActive = c.getString(4);
            boolean ua = false;
            if (strActive.equals("1")){
                ua = true;
            }else if(strActive.equals("0")){
                ua = false;
            }

            User user = new User(c.getLong(0), c.getString(1), c.getString(2), um, ua);
            users.add(user);
            Log.d(TAG, user.toString());
            c.moveToNext();
        }
        c.close();
        return users;
    }

    public User updateUser(User user){
        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_ID, user.getId());
        values.put(COLUMNS_USER_FIRST_NAME, user.getFirstName());
        values.put(COLUMNS_USER_EMAIL, user.getEmail());
        values.put(COLUMNS_USER_MUSIC, user.getFavoriteMusic().toString());
        values.put(COLUMNS_USER_ACTIVE, user.isActive());

        int rowsUpdated = db.update(TABLE_NAME, values, "_id = " + user.getId(), null);

        return user;
    }

    public int deleteItem(User user){
        Integer rowsDeleted = db.delete(TABLE_NAME, COLUMN_USER_ID + " = " + user.getId(), null);
        Log.d(TAG, rowsDeleted.toString());
        return rowsDeleted;
    }

}
