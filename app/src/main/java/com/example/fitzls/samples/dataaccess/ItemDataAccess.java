package com.example.fitzls.samples.dataaccess;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.fitzls.samples.MySQLiteOpenHelper;
import com.example.fitzls.samples.model.Item;

import java.util.ArrayList;

/**
 * Created by SkyFy on 11/13/2017.
 */

public class ItemDataAccess {

    public static final String TAG = "ItemDataAccess";
    private MySQLiteOpenHelper dbHelper;
    private SQLiteDatabase db;

    public static final String TABLE_NAME = "items";
    public static final String COLUMN_ITEM_ID = "_id";
    public static final String COLUMNS_ITEM_NAME = "item_name";

    public static final String TABLE_CREATE =
            String.format("create table %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT)",
                    TABLE_NAME,
                    COLUMN_ITEM_ID,
                    COLUMNS_ITEM_NAME
            );

    public ItemDataAccess(MySQLiteOpenHelper dbHelper){

        this.dbHelper = dbHelper;
        this.db = this.dbHelper.getWritableDatabase();

    }

    public Item insertItem(Item item){

        ContentValues values = new ContentValues();
        values.put(COLUMNS_ITEM_NAME, item.getName());
        long inserId = db.insert(TABLE_NAME, null, values);
        return item;

    }

    public ArrayList<Item> getAllItems(){

        ArrayList<Item> items = new ArrayList<Item>();
        String query = String.format("SELECT %s, %s FROM %s", COLUMN_ITEM_ID, COLUMNS_ITEM_NAME, TABLE_NAME);

        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();
        while(!c.isAfterLast()){
            Item item = new Item(c.getLong(0), c.getString(1));
            items.add(item);
            c.moveToNext();
        }
        c.close();
        return items;
    }

    public Item updateItem(Item item){
        ContentValues values = new ContentValues();
        values.put(COLUMN_ITEM_ID, item.getId());
        values.put(COLUMNS_ITEM_NAME, item.getName());

        int rowsUpdated = db.update(TABLE_NAME, values, "_id = " + item.getId(), null);

        return item;
    }

    public int deleteItem(Item item){
        int rowsDeleted = db.delete(TABLE_NAME, COLUMN_ITEM_ID + " = " + item.getId(), null);
        return rowsDeleted;
    }

}
