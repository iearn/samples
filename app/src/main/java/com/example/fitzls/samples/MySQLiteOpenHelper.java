package com.example.fitzls.samples;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.fitzls.samples.dataaccess.ItemDataAccess;

/**
 * Created by SkyFy on 11/13/2017.
 */

public class MySQLiteOpenHelper extends SQLiteOpenHelper {

    public static final String TAG = "MySQLite";
    public static final String DATA_BASE_NAME = "samples.db";
    public static final int DATABASE_VERSION = 1;

//    If the database doesnt exist it goes to onCreate method.
//    When the Database version is bigger than the last one it calls the onUpgrade.
    public MySQLiteOpenHelper(Context context){
        super(context, DATA_BASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = ItemDataAccess.TABLE_CREATE;
        db.execSQL(sql);
        Log.d(TAG, "Creating db: " + db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
