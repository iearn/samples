package com.example.fitzls.samples;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.util.Log;
import android.widget.Toast;

import com.example.fitzls.samples.dataaccess.UserDataAccess;
import com.example.fitzls.samples.model.User;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Created by SkyFy on 10/23/2017.
 */

public class AppClass extends Application {

    public static final String TAG = "AppClass";
    public String someGlobalVar = "Hey OOO";
    public static ArrayList<User> users = new ArrayList<>();
    public static final String USERS_FILE = "users.dat";

    UserSQLiteOpenHelper dbHelper;
    UserDataAccess da;

    @Override
    public void onCreate() {
        super.onCreate();

//        Only need  it when its the first time adding the users
//        users.add(new User(1,"Bob", "bob@bob.com", User.Music.RAP, true));
//        users.add(new User(2,"Kosin", "Kosin@Mart.com", User.Music.COUNTRY, true));
//        users.add(new User(3, "Ben", "Ben@Ten.com", User.Music.JAZZ, false));
//        wrtieUsersToFile(users, USERS_FILE);

        dbHelper = new UserSQLiteOpenHelper(this);
        da = new UserDataAccess(dbHelper);

        users = da.getAllUsers();
//      Test logging code.
        Log.d(TAG, UserDataAccess.TABLE_CREATE);
        Log.d(TAG, UserDataAccess.SELECT_ALL_STATEMENT);
        Log.d(TAG, users.toString());


//        users = readUserFromFile(USERS_FILE);
    }
    public void updateDB(){
        users = da.getAllUsers();
        Log.d(TAG, da.SELECT_ALL_STATEMENT);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    public void wrtieUsersToFile(ArrayList<User> user, String filePath){
        try {

            FileOutputStream fos = openFileOutput(filePath, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            oos.writeObject(users);

            oos.close();
            fos.close();

        }catch (FileNotFoundException e){
            Log.e(TAG, "File Not Found");
        }catch(IOException e){
            Log.e(TAG, "IO exeception");
        }catch (Exception e){
            Log.e(TAG, "Genral Error");
        }
    }

    public ArrayList<User> readUserFromFile(String filePath){

        ArrayList<User> users = new ArrayList<>();

        try{

            FileInputStream fis = openFileInput(filePath);
            ObjectInputStream ois = new ObjectInputStream(fis);

            users = (ArrayList<User>)ois.readObject();

            ois.close();
            fis.close();

        }catch (FileNotFoundException e){
            Log.e(TAG, "File Not Found");
        }catch(IOException e){
            Log.e(TAG, "IO exeception");
        }catch (Exception e){
            Log.e(TAG, "Genral Error");
        }

        return users;

    }

    public static User getUserById(long id){

        for(User u: users){
            if (u.getId() == id){
                return u;
            }
        }
        return null;

    }

}
