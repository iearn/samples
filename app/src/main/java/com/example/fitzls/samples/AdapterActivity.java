package com.example.fitzls.samples;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fitzls.samples.model.User;

import java.util.ArrayList;

public class AdapterActivity extends AppCompatActivity {

    public static final String TAG = "Adapters";
    ListView listView;
    AppClass app;

    String[] names = {"Bob", "Sally", "Betty"};
    ArrayList<User> users = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adapter);

        app = (AppClass) getApplication();
        Toast.makeText(this, app.someGlobalVar,Toast.LENGTH_LONG).show();

        listView = (ListView) findViewById(R.id.listView);

        users.add(new User(1,"Bob", "bob@bob.com", User.Music.RAP, true));
        users.add(new User(2,"Kosin", "Kosin@Mart.com", User.Music.COUNTRY, true));
        users.add(new User(3,"Ben", "Ben@Ten.com", User.Music.JAZZ, false));

//        example1(); //Uses array of strings as data source
//        example2(); // Uses array of Users as data source
        example3(); //Uses custom view for each item in the users arrraylist
    }

    public void example1(){

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,names);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedString = names[position];
                Toast.makeText( AdapterActivity.this,"selected String: " + selectedString, Toast.LENGTH_LONG).show();

            }
        });
    }

    public void example2(){

        ArrayAdapter<User> adapter = new ArrayAdapter<User>(this, android.R.layout.simple_list_item_1,users);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User selectedUser = users.get(position);
                Toast.makeText( AdapterActivity.this,"Selected User: " + selectedUser, Toast.LENGTH_LONG).show();

            }
        });

    }

    public void example3(){

        ArrayAdapter<User> adapter = new ArrayAdapter<User>(this, R.layout.activity_custom_user_list_item, R.id.lblFirstName, users){

            @Override
            public View getView(int position, View view, ViewGroup parent){

                View itemView = super.getView(position, view, parent);

                ListView listView = (ListView)parent;
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        User selectedUser = users.get(position);
                    }
                });

                TextView lblFirstName = (TextView) itemView.findViewById(R.id.lblFirstName);
                CheckBox chkActive = (CheckBox)itemView.findViewById(R.id.chkActive);

                User u = users.get(position);
                lblFirstName.setText(u.getFirstName());
                chkActive.setChecked(u.isActive());

//              Set tag is a way to bind data to something
                chkActive.setTag(u);

                chkActive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        CheckBox pressedCheckBox = (CheckBox) v;
                        User u = (User)pressedCheckBox.getTag();

                        u.setActive(pressedCheckBox.isChecked());

                        Toast.makeText(AdapterActivity.this, u.toString(), Toast.LENGTH_LONG).show();

                    }
                });

                return itemView;

            }
        };

        listView.setAdapter(adapter);
    }


}
