package com.example.fitzls.samples;

import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;

public class TempMainActivity extends AppCompatActivity {

    Button btnImgSpinner, btnUserD, btnIntentS, btnAdapter, btnLifeCycle, btnCoder, btnWebService, btnSQLite;


    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            int buttonRes = v.getId();
            Intent i = null;

            switch(buttonRes){
                case R.id.btnImgSpinner:
                    i = new Intent(TempMainActivity.this, ImageSpinnerActivity.class);
                    break;

                case R.id.btnUserListView:
                    i = new Intent(TempMainActivity.this, UserListActivity.class);
                    break;

                case R.id.btnIntentSender:
                    i = new Intent(TempMainActivity.this, IntentSenderActivity.class);
                    break;

                case R.id.btnAdapter:
                    i = new Intent(TempMainActivity.this, AdapterActivity.class);
                    break;

                case R.id.btnLifeCycle:
                    i = new Intent(TempMainActivity.this, LifeCycleActivity.class);
                    break;

                case R.id.btnCoder:
                    i = new Intent(TempMainActivity.this, CoderActivity.class);
                    break;

                case R.id.btnWebService:
                    i = new Intent(TempMainActivity.this, WebServiceActivity.class);
                    break;

                case R.id.btnSQLite:
                    i = new Intent(TempMainActivity.this, SQLiteActivity.class);
                    break;

            }
            startActivity(i);


        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp_main);

        btnImgSpinner = (Button) findViewById(R.id.btnImgSpinner);
        btnUserD = (Button) findViewById(R.id.btnUserListView);
        btnIntentS = (Button)findViewById(R.id.btnIntentSender);
        btnAdapter = (Button) findViewById(R.id.btnAdapter);
        btnLifeCycle = (Button)findViewById(R.id.btnLifeCycle);
        btnCoder = (Button)findViewById(R.id.btnCoder);
        btnWebService = (Button)findViewById(R.id.btnWebService);
        btnSQLite = (Button)findViewById(R.id.btnSQLite);

        btnImgSpinner.setOnClickListener(listener);
        btnUserD.setOnClickListener(listener);
        btnIntentS.setOnClickListener(listener);
        btnAdapter.setOnClickListener(listener);
        btnLifeCycle.setOnClickListener(listener);
        btnCoder.setOnClickListener(listener);
        btnWebService.setOnClickListener(listener);
        btnSQLite.setOnClickListener(listener);
    }

}