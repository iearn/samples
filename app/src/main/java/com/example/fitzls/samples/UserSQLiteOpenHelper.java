package com.example.fitzls.samples;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.fitzls.samples.dataaccess.ItemDataAccess;
import com.example.fitzls.samples.dataaccess.UserDataAccess;

/**
 * Created by SkyFy on 11/15/2017.
 */

public class UserSQLiteOpenHelper extends SQLiteOpenHelper {

    public static final String TAG = "UserSQLite";
    public static final String DATA_BASE_NAME = "users.db";
    public static final int DATABASE_VERSION = 1;

    public UserSQLiteOpenHelper(Context context){
        super(context, DATA_BASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = UserDataAccess.TABLE_CREATE;
        db.execSQL(sql);
        Log.d(TAG, sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
