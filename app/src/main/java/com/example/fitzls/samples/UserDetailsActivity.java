package com.example.fitzls.samples;

import android.app.Application;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.fitzls.samples.dataaccess.UserDataAccess;
import com.example.fitzls.samples.model.User;

import java.io.Console;

public class UserDetailsActivity extends AppCompatActivity {

    public static final String TAG = "USER_MUSIC";
    public static final String USER_ID_EXTRA = "userid";

    AppClass appClass = new AppClass();

    private User u = new User();
    private EditText firstName, email;
    private RadioButton country, jazz, rap;

    private RadioGroup radioGroupMusic;
    private RadioButton radioButton;

    private ToggleButton toggleBtn;

    private Button btnSave;

    UserSQLiteOpenHelper dbHelper;
    UserDataAccess da;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        appClass = (AppClass) getApplication();

        dbHelper = new UserSQLiteOpenHelper(this);
        da = new UserDataAccess(dbHelper);

        firstName   = (EditText)findViewById(R.id.firstName);
        email   = (EditText)findViewById(R.id.email);
        radioGroupMusic = (RadioGroup) findViewById(R.id.radioGroupMusic);
        toggleBtn = (ToggleButton)findViewById(R.id.toggleBtn);

        Intent i = getIntent();
//        Second parameter used if it fails
        final Long userId = i.getLongExtra(USER_ID_EXTRA, -1);
        if (userId >= 0){
            u = appClass.getUserById(userId);

//            Toast.makeText(UserDetailsActivity.this, u.toString(), Toast.LENGTH_LONG).show();

            fillData();
        }

        btnSave = (Button)findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                User nU = new User();
                nU = getDataFromUI(userId);

//                If the user is valid then update or insert the new user
                if (validateUserInfo(nU)){
//                    A new user doesnt have an userId
                    if (userId >= 0){
                        da.updateUser(nU);
                    }else{
                        da.insertUser(nU);
                    }
//                    Go back to the UserList Activity
                    Intent i = new Intent(UserDetailsActivity.this, UserListActivity.class);
                    startActivity(i);
                }

            }
        });
    }

    private boolean validateUserInfo(User validateUser){

        boolean isValid = true;

        if (validateUser.getFirstName().equals("")){
            isValid = false;
            Toast.makeText(UserDetailsActivity.this, "User has to have a name", Toast.LENGTH_LONG).show();
        }

        if (validateUser.getEmail().equals("")){
            isValid = false;
            Toast.makeText(UserDetailsActivity.this, "User has to have a email", Toast.LENGTH_LONG).show();
        }

        if (validateUser.getFavoriteMusic() == null)
        {
            isValid = false;
            Toast.makeText(UserDetailsActivity.this, "User has to have a music", Toast.LENGTH_LONG).show();
        }

        return isValid;
    }

    private User getDataFromUI(long userId){

        String fName = firstName.getText().toString();
        String eMail = email.getText().toString();

        RadioGroup musicGroup = radioGroupMusic;
        int selectedMusicId = musicGroup.getCheckedRadioButtonId();

        // find the radiobutton by returned id
        radioButton = (RadioButton) findViewById(selectedMusicId);
        User.Music userMusic = User.Music.RAP;

//        If the userMusic is null then the validation will catch it.
        if (radioGroupMusic.getCheckedRadioButtonId() > -1){

            if (radioButton.getText().equals("Rap")){
                userMusic = User.Music.RAP;
            }else if (radioButton.getText().equals("Country")){
                userMusic = User.Music.COUNTRY;
            }else if (radioButton.getText().equals("Jazz")){
                userMusic = User.Music.JAZZ;
            }

        }else{
            userMusic = null;
        }

        boolean toggledBtn = toggleBtn.isChecked();

        User newUser = new User(userId,fName, eMail, userMusic,toggledBtn);

        return newUser;
    }

    private void fillData(){

        firstName.setText(u.getFirstName(), TextView.BufferType.EDITABLE);;
        email.setText(u.getEmail(), TextView.BufferType.EDITABLE);;

        switch(u.getFavoriteMusic()){

            case JAZZ:
                radioGroupMusic.check(R.id.radioBtnJazz);
                break;

            case COUNTRY:
                radioGroupMusic.check(R.id.radioBtnCountry);
                break;

            case RAP:
                radioGroupMusic.check(R.id.radioBtnRap);
                break;

        }

        toggleBtn.setChecked(u.isActive());

    }
}
