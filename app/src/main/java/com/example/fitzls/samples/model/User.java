package com.example.fitzls.samples.model;

import java.io.Serializable;

/**
 * Created by SkyFy on 9/18/2017.
 */

public class User implements Serializable {

    public enum Music{
        COUNTRY{
        @Override
        public String toString(){
            return "Country";
        }
        },
        RAP{
            @Override
            public String toString(){
                return "Rap";
            }
        },
        JAZZ{
            @Override
            public String toString(){
                return "Jazz";
            }
        }
    };

    private String firstName;
    private String email;
    public Music favoriteMusic;
    private boolean active;

    private long id;

//    Constructors
    public User(){
    }

    public User(long id, String firstName, String email, Music favoriteMusic, boolean active) {
        this.id = id;
        this.firstName = firstName;
        this.email = email;
        this.favoriteMusic = favoriteMusic;
        this.active = active;
    }

    // getter and setters
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Music getFavoriteMusic() {
        return favoriteMusic;
    }

    public void setFavoriteMusic(Music favoriteMusic) {
        this.favoriteMusic = favoriteMusic;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString(){
        return String.format("ID: %d FName: %s Email: %s Music %s Active: %b", id, firstName, email, favoriteMusic, active);
    }
}
