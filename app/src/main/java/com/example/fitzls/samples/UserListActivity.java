package com.example.fitzls.samples;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fitzls.samples.dataaccess.UserDataAccess;
import com.example.fitzls.samples.model.User;

public class UserListActivity extends AppCompatActivity {

    public static final String TAG = "UserListActivty";
    UserSQLiteOpenHelper dbHelper;
    UserDataAccess da;

    AppClass app;
    ListView userListView;

    Button btnAddUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);

        btnAddUser = (Button)findViewById(R.id.btnAddUser);
        btnAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(UserListActivity.this, UserDetailsActivity.class);
                startActivity(i);
            }
        });

        app = (AppClass) getApplication();
        userListView = (ListView) findViewById(R.id.userListView);

/*      Class test code.
        ArrayAdapter<User> adapter = new ArrayAdapter<User>(this, android.R.layout.simple_list_item_1, app.users);

        userListView.setAdapter(adapter);
        userListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User selectedUser = app.users.get(position);
//                Toast.makeText(UserListActivity.this, selectedUser.getFirstName(), Toast.LENGTH_LONG).show();
                Intent i = new Intent(UserListActivity.this, UserDetailsActivity.class);
                i.putExtra(UserDetailsActivity.USER_ID_EXTRA, position);

                startActivity(i);
            }
        });
*/
//  refreshes the User List with up to date array.
    app.updateDB();

    ArrayAdapter adapter = new ArrayAdapter<User>(UserListActivity.this,
            R.layout.activity_custom_user_list_item, R.id.lblFirstName, app.users){

        @Override
        public View getView(int position, View convertView, ViewGroup parent){

            View listItemView = super.getView(position, convertView, parent);

            final User currentUser;
            currentUser = app.users.get(position);
            listItemView.setTag(currentUser);

            TextView lbl = (TextView)listItemView.findViewById(R.id.lblFirstName);
            CheckBox chk = (CheckBox)listItemView.findViewById(R.id.chkActive);
            Button del = (Button) listItemView.findViewById(R.id.btnDelete);

            lbl.setText(currentUser.getFirstName());
            chk.setChecked(currentUser.isActive());
            chk.setText("Active");
            chk.setTag(currentUser);

            chk.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view){
                    User selectedUser = (User)view.getTag();
                    selectedUser.setActive(((CheckBox)view).isChecked());
                }
            });

            listItemView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view){
                    User selectedUser = (User) view.getTag();
                    Intent i = new Intent(UserListActivity.this, UserDetailsActivity.class);
                    i.putExtra(UserDetailsActivity.USER_ID_EXTRA, selectedUser.getId());
                    startActivity(i);
                }
            });

            del.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    app.da.deleteItem(currentUser);
                    finish();
                    startActivity(getIntent());
                }
            });

            return listItemView;

        }

    };

    userListView.setAdapter(adapter);

    }

}
